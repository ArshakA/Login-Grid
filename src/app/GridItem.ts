export class GridItem {
  id: number;
  title: string;
  text: string;
  lang: string;
  date: Date;
}
