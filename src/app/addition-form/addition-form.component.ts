import {Component, Input, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {GridItem} from '../GridItem';
import {ItemService} from '../item.service';
import {ActivatedRoute} from '@angular/router';


@Component({
  selector: 'app-addition-form',
  templateUrl: './addition-form.component.html',
  styleUrls: ['./addition-form.component.css']
})
export class AdditionFormComponent implements OnInit {
  model: any = {lang: 'English' };

  constructor(private itemService: ItemService,
              private location: Location) {
  }

  ngOnInit() {
  }

  goBack(): void {
    this.location.back();
  }

  addItem(title: string, text: string, lang: string, date: Date): void {
    this.itemService.addItem({title, text, lang, date} as GridItem)
      .subscribe(() => this.goBack());
  }
}

