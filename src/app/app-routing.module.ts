import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {GridComponent} from './grid/grid.component';
import {ItemDetailsComponent} from './item-details/item-details.component';
import {AdditionFormComponent} from './addition-form/addition-form.component';
import {LoginComponent} from './login/login.component';


const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'gridItems/additionForm', component: AdditionFormComponent},
  {path: 'gridItems/:id', component: ItemDetailsComponent},
  {path: 'gridItems', component: GridComponent},
  {path: '**', redirectTo: '/login', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
