import {Component, OnInit, Input} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import {GridItem} from '../GridItem';
import {ItemService} from '../item.service';


@Component({
  selector: 'app-item-details',
  templateUrl: './item-details.component.html',
  styleUrls: ['./item-details.component.css']
})
export class ItemDetailsComponent implements OnInit {


  @Input() gridItem: GridItem;

  constructor(private route: ActivatedRoute,
              private itemService: ItemService,
              private location: Location) {
  }

  ngOnInit(): void {
   //this.getItem();
  }

  getItem(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.itemService.getGridItem(id)
      .subscribe(gridItem => this.gridItem = gridItem);
  }

  hide(): void {
    this.gridItem = undefined;
  }

  save(): void {
    this.itemService.editItem(this.gridItem)
      .subscribe(() => this.hide());
  }

}
