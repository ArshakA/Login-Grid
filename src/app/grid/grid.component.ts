import {Component, OnInit} from '@angular/core';
import {GridItem} from '../GridItem';
import {ItemService} from '../item.service';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.css']
})
export class GridComponent implements OnInit {
  gridItem: GridItem;
  gridItems: GridItem[];
  selectedItem: GridItem;

  constructor(private itemService: ItemService) {
  }

  ngOnInit() {
    this.getGridItems();
  }

  getGridItems(): void {
    this.itemService.getGridItems()
      .subscribe(gridItems => this.gridItems = gridItems);
  }

  getGridItem(): void {
    this.itemService.getGridItem(this.gridItem.id).subscribe(gridItem => this.gridItem = gridItem);
  }

  /* addItem(title: string, text: string, lang: string, date: Date): void {
    name = name.trim();
    if (!name) {
      return;
    }
    this.heroService.addHero({name} as Hero)
      .subscribe(hero => {
        this.heroes.push(hero);
      });
  }
*/
  deleteItem(gridItem: GridItem): void {
    this.gridItems = this.gridItems.filter(g => g !== gridItem);
    this.itemService.deleteItem(gridItem).subscribe();
    this.getGridItems();
  }

  editGridItem(item): void {
    this.selectedItem = item;
    console.log('edit item');
  }
}
