import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';
import {GridItem} from './GridItem';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root',
})
export class ItemService {
  private gridItemsUrl = 'api/gridItems';

  constructor(private http: HttpClient) {
  }

  /** GET heroes from the server */
  getGridItems(): Observable<GridItem[]> {
    return this.http.get<GridItem[]>(this.gridItemsUrl);
  }

  getGridItem(id: number): Observable<GridItem> {
    const url = `${this.gridItemsUrl}/${id}`;
    return this.http.get<GridItem>(url);
  }

  addItem(gridItem: GridItem): Observable<GridItem> {
    return this.http.post<GridItem>(this.gridItemsUrl, gridItem, httpOptions);
  }

  /** DELETE: delete the gridItem from the server */
  deleteItem(gridItem: GridItem | number): Observable<GridItem> {
    const id = typeof gridItem === 'number' ? gridItem : gridItem.id;
    const url = `${this.gridItemsUrl}/${id}`;

    return this.http.delete<GridItem>(url, httpOptions).pipe();
  }

  /** PUT: update the gridItem on the server */
  editItem(gridItem: GridItem): Observable<any> {
    return this.http.put(this.gridItemsUrl, gridItem, httpOptions);
  }
}

