import {InMemoryDbService} from 'angular-in-memory-web-api';

//import {LoginDetails} from './LoginDetails';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const gridItems = [
      {id: 0, title: 'vzgo', text: 'vagho', lang: 'English', date: new Date(1535058000000)},
      {id: 1, title: 'psko', text: 'vagho', lang: 'English', date: new Date(1535058000000)},
      {id: 2, title: 'tsko', text: 'vagho', lang: 'English', date: new Date(1535058000000)},
    ];

    const loginDetails = {
      id: 1,
      userName: 'user',
      password: 'pass',
      firstName: 'Arshak',
      lastName: 'Amirjanyan',
      token: 'fake-jwt-token'
    };
    return {gridItems, loginDetails};
  }
}
