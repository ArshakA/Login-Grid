import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../authentication.service';
import {LoginDetails} from '../LoginDetails';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  model: any = {};
  loginDetails: any = {
  };
  clicked = false;

  constructor(private auth: AuthenticationService, private router: Router) {
  }

  ngOnInit() {
    this.auth.login()
      .subscribe(details => this.loginDetails = details);
  }

  login(user, pass): void {
    this.clicked = true;
    if (user === this.loginDetails.userName && pass === this.loginDetails.password) {
      this.router.navigate(['/gridItems']);
    }
  }
}
