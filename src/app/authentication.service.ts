import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {LoginDetails} from './LoginDetails';
import {GridItem} from './GridItem';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private userUrl = 'api/loginDetails';

  constructor(private http: HttpClient) {
  }

  login(): Observable<LoginDetails[]> {
      return this.http.get<LoginDetails[]>(this.userUrl);
  }
}
