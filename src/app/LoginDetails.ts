export class LoginDetails {
  id: number;
  username: string;
  firstName: string;
  lastName: string;
  password: string;
  token: string;
}
